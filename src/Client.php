<?php

namespace ARIA\MolgenisPhpClient;

use GuzzleHttp\Client as http;
use RuntimeException;

/**
 * V2 API endpoint for Molgenis databases
 */
class Client
{

    private string $endpoint = '';
    private string $token = '';
    private string $username = '';
    private string $password = '';

    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';


    public function __construct(string $endpoint)
    {
        $this->setEndpoint($endpoint);
    }

    public function setEndpoint(string $url)
    {
        $url = trim($url, ' /') . '/';
        $this->endpoint = $url;
    }

    public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    /**
     * Set backend generated access token (takes precedence over username and password)
     * @param string $token Set authentication token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }

    /**
     * Set access user and password
     *
     * @param string $username
     * @param string $password
     * @return void
     */
    public function setUserPass(string $username, string $password) {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Get an entity from endpoint with optional parameters
     *
     * @param string $entity
     * @param string $entity_id
     * @param array $attrs
     * @return void
     */
    public function get(string $entity, string $entity_id = '', array $attrs = []) {
        return $this->call(
            query: "{$entity}/{$entity_id}",
            params: $attrs
        );
    }

    /**
     * Post to an endpoint
     * 
     * @param string $entity
     * @param string $entity_id
     * @param string $body
     * @return void
     */
    public function post(string $entity, string $entity_id = '', string $body = null) {
        return $this->call(
            query: "{$entity}/{$entity_id}",
            body: $body,
            method: Client::METHOD_POST
        );
    }

    /**
     * Put to an endpoint
     * 
     * @param string $entity
     * @param string $entity_id
     * @param string $body
     * @return void
     */
    public function put(string $entity, string $entity_id = '', string $body = null) {
        return $this->call(
            query: "{$entity}/{$entity_id}",
            body: $body,
            method: Client::METHOD_PUT
        );
    }

    /**
     * Delete an entity
     *
     * @param string $entity
     * @param string $entity_id
     * @return void
     */
    public function delete(string $entity, string $entity_id = '') {
        return $this->call(
            query: "{$entity}/{$entity_id}",
            method: Client::METHOD_DELETE
        );
    }

    /**
     * Low level execution of query
     *
     * @param string $query
     * @param array $params
     * @param array $headers
     * @param string $body
     * @param [type] $method
     * @return void
     */
    protected function call(string $query, array $params = [], array $headers = [], string $body = null,  string $method = Client::METHOD_GET)
    {

        $client = new http();

        $request_params = [
            'form_params' => $body,
            'query' => $params
        ];

        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];

        // Set the access 
        if (!empty($this->token)) {
            $headers['x-molgenis-token'] = $this->token;
        } else if (!empty($this->username) && !empty($this->password)) {
            $request_params['auth'] = [
                $this->username, $this->password
            ];
        }

        $request_params['headers'] = $headers;
        
        $body = [];
        
        // Catch a nulled endpoint
        if (empty($this->endpoint)) {
            throw new RuntimeException('No Molgenis endpoint specified for this client');
        }

        $response = $client->request($method, $this->endpoint . $query, $request_params);
        
        $responsebody = json_decode($response->getBody(), true);

        // Todo analyse result

        // Todo analyse return code
        
        
        return $responsebody;
    }
}
