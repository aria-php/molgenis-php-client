<?php

use ARIA\MolgenisPhpClient\Client;
use \PHPUnit\Framework\TestCase;

class ClientTest extends TestCase {

    public function testConnection() 
    {

        $client = new Client($_ENV['ENDPOINT']);

        if (!empty($_ENV['CLIENT']) && !empty($_ENV['PASSWORD'])) {
            $client->setUserPass($_ENV['CLIENT'], $_ENV['PASSWORD']);
        } else if (!empty($_ENV['TOKEN'])) {
            $client->setToken($_ENV['TOKEN']);
        }

        $result = $client->get($_ENV['ENTITY']);

        $this->assertNotEmpty($result);

        var_export($result);
    }



}