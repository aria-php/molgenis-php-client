# Molgenis PHP Client

This is a simple PHP client for interfacing with a Molgenis database endpoint.

## Installation

`composer require aria-php/molgenis-php-client`

## Usage

The `Client` provides the low level interface for calling a Molgenis server


```

use ARIA\MolgenisPhpClient\Client

$client = new Client('https://molgenis.example.com/');

$result = $client->get('entity_type', 'example_entity_id');

var_export($result);

```


## Running tests

Create a `.env.local` in the `tests` directory containing the endpoint and entity information that you are going to query.